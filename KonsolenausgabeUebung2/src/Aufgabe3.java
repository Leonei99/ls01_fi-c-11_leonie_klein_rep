
public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String t1 = "Fahrenheit";
		String t2 = "Celsius";
		String a1= "-20";
		double a2= -28.8889;
		String b1= "-10";
		double b2= -23.3333;
		String c1= "+0";
		double c2=-17.7778;
		String d1="+20";
		double d2= -6.6667;
		String e1= "+30";
		double e2= -1.1111;
		
		System.out.printf("%-12s | %10s\n", t1, t2);
		System.out.println("--------------------------");
		System.out.printf("%-12s | %10.6s\n", a1, a2);
		System.out.printf("%-12s | %10.6s\n", b1, b2);
		System.out.printf("%-12s | %10.6s\n", c1, c2);
		System.out.printf("%-12s | %10.6s\n", d1, d2);
		System.out.printf("%-12s | %10.6s\n", e1, e2);
		
	

	}

}
