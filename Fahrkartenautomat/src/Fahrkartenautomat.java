import java.util.Scanner;

class Fahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);

//Methoden
	public static float fahrkartenbestellungErfassen() {
		int ticketWahl = 0;
		float zuZahlenderBetrag = 0;
		System.out.println("Willkommen. Es gibt folgende Tickets: ");
		System.out.println(" \n 1 Einzelfahrt f�r 2,90� \n 2 Tageskarte f�r 8,60� \n 3 Gruppenkarte f�r 23,50�");
		System.out.println("Geben Sie die Nummer des Tickets ein, das Sie bestellen wollen:");
		while (ticketWahl != 1 && ticketWahl != 2 && ticketWahl != 3) {
			ticketWahl = tastatur.nextInt();
			switch (ticketWahl) {
			case 1:
				System.out.println("Einzelfahrt f�r 2,90�");
				zuZahlenderBetrag = 2.90f;
				break;
			case 2:
				System.out.println("Tageskarte f�r 8,60�");
				zuZahlenderBetrag = 8.60f;
				break;
			case 3:
				System.out.println("Gruppenkarte f�r 23,50�");
				zuZahlenderBetrag = 23.50f;
				break;
			default:
				System.out.println("Sie haben eine falsche Zahl eingegeben. Geben Sie 1, 2 oder 3 ein.");
				break;
			}
		}

		return zuZahlenderBetrag;
	}

	public static float gesamtpreis(float zuZahlenderBetrag) {
		float zuZahlen;
		float anzahlTickets;
		System.out.println("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextFloat();

		while (anzahlTickets >= 11) {
			System.out.println("Es k�nnen maximal 10 Tickets gekauft werden.");
			System.out.println("neue Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextFloat();
		}
		while (anzahlTickets <= 0) {
			System.out.println("Es muss mind. 1 Ticket gekauft werden.");
			System.out.println("neue Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextFloat();

		}
		zuZahlen = anzahlTickets * zuZahlenderBetrag;
		return zuZahlen;
	}

	public static float fahrkartenBezahlen(float zuZahlen) {
		int eingezahlterGesamtbetrag = 0;
		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f", ((zuZahlen - eingezahlterGesamtbetrag)));
			System.out.print("Euro\n");
			System.out.println("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			float eingeworfeneM�nze = tastatur.nextFloat();

			while (eingeworfeneM�nze > 2) {
				System.out.println("Bitte maxmimal 2 Euro einwerfen.");
				System.out.println("Neuer M�nzeinwurf: ");
				eingeworfeneM�nze = tastatur.nextFloat();

			}
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		float rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
		return rueckgabebetrag;
	}

	public static void warte(int millisekunde) {
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void fahrkartenAusgeben() {
		int millisekunde = 90;
		Fahrkartenautomat.warte(millisekunde);
		System.out.println("\nFahrschein wird ausgegeben");
	}

	public static void rueckgeldAusgeben(float r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0f) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f", r�ckgabebetrag);
			System.out.print(" EURO ");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 1.9f) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0f;
			}
			while (r�ckgabebetrag >= 0.9f) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0f;
			}
			while (r�ckgabebetrag >= 0.49f) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5f;
			}
			while (r�ckgabebetrag >= 0.19f) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2f;
			}
			while (r�ckgabebetrag >= 0.09f) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1f;
			}
			while (r�ckgabebetrag >= 0.049f)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05f;
			}
		}
	}

	public static void main(String[] args) {

		while (true) {
			float ticketWahl = Fahrkartenautomat.fahrkartenbestellungErfassen();
			float preis = Fahrkartenautomat.gesamtpreis(ticketWahl);
			float r�ckgabebetrag = Fahrkartenautomat.fahrkartenBezahlen(preis);
			Fahrkartenautomat.fahrkartenAusgeben();
			Fahrkartenautomat.rueckgeldAusgeben(r�ckgabebetrag);

			System.out.println("\nVergessen Sie bitte nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt. \n\n");
		}
	}
}

//A2.5
//double zuZahlenderBetrag, double eingezahlterGesamtbetrag (diese beiden werden subtrahiert), 
//double r�ckgabebetrag (wird ausgegeben)
//double eingeworfeneM�nze
//int anzahlTickets User kann Anzahl der Tickets eingeben, wird multipliziert
//Ich habe hierf�r (die Ticketanzahl) einen int genutzt, da die Anzahl der Tickets immer eine Ganzzahl ist 
//und niemals gr��er als 2Millionen sein wird (sowieso niemals gr��er als 4, da das meine festgelegte Maximalanzahl
//an Tickets ist.
// Operatoren: = f�r Zuweisungen, > und < zum Vergleichen von Gr��en, + zum Verbinden
