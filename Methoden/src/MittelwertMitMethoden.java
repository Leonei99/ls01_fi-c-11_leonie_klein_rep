import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		double mittelwert;
		Scanner myScanner = new Scanner(System.in);
		zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
		zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ");
		mittelwert = mittelwertBerechnung(zahl1,zahl2);
		ausgabe(mittelwert);
		myScanner.close();
		
	}
	public static double eingabe(Scanner myScanner, String text) {
		
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	public static double mittelwertBerechnung(double zahl1, double zahl2) {
		double mittelwert = (zahl1 + zahl2)/2.0;
		return mittelwert;
	}
	public static void ausgabe(double mittelwert) {
		System.out.println(mittelwert);
	}
}
