import java.util.Scanner;

public class Parallelschaltung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner =new Scanner(System.in);
		double r1 = eingabe(myScanner, "Widerstand 1 in Ohm: ");
		double r2 = eingabe(myScanner, "Widerstand 2 in Ohm: ");
		System.out.print("Ersatzwiderstand in Ohm: ");
		System.out.println(parallelschaltung(r1,r2));
	}
	public static double eingabe(Scanner myScanner, String text) {
		
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		double ersatzwiderstand = (r1*r2)/(r1+r2);
		return ersatzwiderstand;
	}



	}


