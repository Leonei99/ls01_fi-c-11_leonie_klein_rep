import java.util.Scanner;

public class Fahrsimulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		double v=0.0;
		v = eingabe(myScanner, "Geschwindigkeit zwischen 0 und 130 km/h eingeben: ");
		if (v < 0.0) {
			System.out.println("HALLOOOOOOO ZWISCHEN 0 UND 130 NICHT DRUNTER");
		}
		if (v > 130.0) {
			System.out.println("HALLOOOOOOO ZWISCHEN 0 UND 130 NICHT MEHR");
		}
		double dv = eingabe(myScanner, "Veränderung der Geschwindigkeit eingeben: ");
		double endg = beschleunige(v,dv);
		ausgabe(endg);
	}
	public static double beschleunige(double v, double dv) {
		double beschleunigung = v/dv;
		return beschleunigung;
	}
	
	public static double eingabe(Scanner myScanner, String text) {
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	
	public static void ausgabe(double endg) {
		System.out.print("Die Endgeschwindigkeit lautet: ");
		System.out.println(endg);
	}
}
