import java.util.Scanner;
public class Mittelwert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner meinScanner= new Scanner(System.in);
		// (E) "Eingabe"
	      // Werte f�r x und y festlegen:
	      // ===========================
	      double x ;
	      double y ;
	      double m;
	      System.out.println("Bitte geben Sie die erste Zahl ein: ");
	      x = meinScanner.nextDouble();
	      
	      System.out.println("Bitte geben Sie die zweite Zahl ein: ");
	      y = meinScanner.nextDouble();
	      
	      // (V) Verarbeitung
	      // Mittelwert von x und y berechnen: 
	      // ================================
	      m = (x + y) / 2.0;
	      
	      // (A) Ausgabe
	      // Ergebnis auf der Konsole ausgeben:
	      // =================================
	      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	}

}
