import java.util.Scanner;

public class PCHaendler {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		//String text = myScanner.next();
		String artikel = liesString(myScanner, "hallo");
		int anzahl = liesInt(myScanner, "halloo");
		double nettopreis = liesDouble(myScanner, "he");
		double mwst = mwstsatz(myScanner, "ree");
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
		// Benutzereingaben lesen
		public static String liesString(Scanner myScanner, String text) {
			System.out.println("Was wollen Sie bestellen?");
			String artikel = myScanner.next();
			return artikel;
		}
		public static int liesInt(Scanner myScanner, String text) {
			System.out.println("Geben Sie die Anzahl ein:");
			int anzahl = myScanner.nextInt();
			return anzahl;
		}
		public static double liesDouble(Scanner myScanner, String text) {
			System.out.println("Geben Sie den Nettopreis ein:");
			double nettopreis = myScanner.nextDouble();
			return nettopreis;
		}
		public static double mwstsatz(Scanner myScanner, String text) {
			System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
			double mwst = myScanner.nextDouble();
			return mwst;
		}

		// Verarbeiten
		public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
			double nettogesamtpreis = anzahl * nettopreis;
			return nettogesamtpreis;
		}
		public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
			double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			return bruttogesamtpreis;
		}
		// Ausgeben
		public static void rechungausgeben(String artikel, int anzahl, double
				nettogesamtpreis, double bruttogesamtpreis,
				double mwst) {

			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		}
	}


