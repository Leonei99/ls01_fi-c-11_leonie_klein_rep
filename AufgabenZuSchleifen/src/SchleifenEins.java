import java.util.Scanner;

public class SchleifenEins {

	public static void main(String[] args) {

		Scanner myScan = new Scanner(System.in);
		int i = 1;
		int fak = 1;
		System.out.println("Geben Sie die Zahl ein, deren Fakult�t Sie berechnen wollen:");
		int userZahl = myScan.nextInt();
		while (i <= userZahl) {
			fak = fak*i;
			i++;
		}
		System.out.println("Die Fakult�t von "+ userZahl + " lautet: "+ fak);
	}
}
