
public class SchleifenABEins {

	public static void main(String[] args) {
		
		System.out.println("Willkommen zum Programm, welches das kleine Einmaleins ausgibt!");
		System.out.println();
		for (int zaehler = 1; zaehler <= 10; zaehler++) {
			for (int i = 1; i <= 10; i++) {
		        System.out.print("   " + zaehler * i);  
			}
			System.out.println();
			System.out.println();
			System.out.println();
		}

	}

}
