import java.util.Scanner;

public class OhmschesGesetz 
{
	public static void main(String[] args) 
	{
		Scanner myScan = new Scanner(System.in);
		float spannung;
		float strom;
		float widerstand;
		System.out.println("Das Ohmsche Gesetz lautet: U = R* I");
		System.out.println("Geben Sie ein, welche Gr��e Sie berechnen wollen.");
		char groesse = myScan.next().charAt(0);
		
		switch(groesse) 
		{
		case 'R':
		case 'r':
			System.out.println("Sie wollen den Widerstand R berechnen.");
			System.out.println("Geben Sie hierf�r zun�chst die Spannung U in Volt ein:");
			spannung = myScan.nextFloat();
			System.out.println("Geben Sie nun den Strom I in Ampere an:");
			strom = myScan.nextFloat();
			widerstand = spannung/strom;
			System.out.println("Der Widerstand R ist "+ widerstand+ " Ohm gro�.");
			break;
		case 'U':
		case 'u':
			System.out.println("Sie wollen die Spannung U berechnen.");
			System.out.println("Geben Sie hierf�r zun�chst den Strom I in Ampere an:");
			strom = myScan.nextFloat();
			System.out.println("Geben Sie nun den Widerstand R in Ohm ein:");
			widerstand = myScan.nextFloat();
			spannung = strom*widerstand;
			System.out.println("Die Spannung U ist " + spannung+ " Volt gro�.");
			break;
		case 'I':
		case 'i':
			System.out.println("Sie wollen den Strom I berechnen.");
			System.out.println("Geben Sie hierf�r zun�chst die Spannung U in Volt ein:");
			spannung = myScan.nextFloat();
			System.out.println("Geben Sie nun den Widerstand R in Ohm ein:");
			widerstand = myScan.nextFloat();
			strom = spannung/widerstand;
			System.out.println("Der Strom I ist " + strom + " Ampere gro�.");
			break;
		default:
			System.out.println("Falsche Eingabe. Bitte geben Sie entweder R, U oder I ein.");			
		}
		myScan.close();
	}
}
