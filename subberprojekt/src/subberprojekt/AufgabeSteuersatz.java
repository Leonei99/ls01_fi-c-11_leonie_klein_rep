package subberprojekt;
import java.util.Scanner;

public class AufgabeSteuersatz {
	public static void main(String[] args) {
		Scanner myScan = new Scanner(System.in);
		System.out.println("Geben Sie den Nettopreis in Euro des Produktes ein: ");
		float preis = myScan.nextFloat();
		System.out.println("Jetzt k�nnen Sie den Steuersatz ausw�hlen.");
		System.out.println("Geben Sie ein: j f�r erm��igten Steuersatz (7%) oder n f�r vollen Steuersatz (19%).");
		String userAuswahl = myScan.next();
		if (userAuswahl.equals("j")) {
			float steuerErm = 0.07f;
			float ergebnis = preis + (steuerErm*preis);
			System.out.println("Dies ist der Bruttopreis mit Steuer: ");
			System.out.println(ergebnis);
		}
		else if (userAuswahl.equals("n")) {
			float steuerVoll = 0.19f;
			float ergebnis = preis + (steuerVoll*preis);
			System.out.println("Dies ist der Bruttopreis mit Steuer: ");
			System.out.println(ergebnis);
		}
		else {
			System.out.println("Es ist ein Fehler aufgetreten.");
		}
		myScan.close();
	}


}
