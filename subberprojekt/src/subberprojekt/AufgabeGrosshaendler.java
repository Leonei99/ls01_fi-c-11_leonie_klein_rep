package subberprojekt;
import java.util.Scanner;

public class AufgabeGrosshaendler {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Hallo. Wie viele M�use wollen Sie?");
		float mausAnzahl = scan.nextFloat();
		System.out.println("Wie viel kostet ein Maus?");
		float preisMaus = scan.nextFloat();
		float mehrwertSt = 0.19f;
		
		if (mausAnzahl < 10) {
			System.out.println("Die Lieferung kostet 10 Euro.");
			float preisGes = mausAnzahl *(preisMaus+(preisMaus*mehrwertSt))+10.00f;
			System.out.println("Der Gesamtpreis mit Steuer ist " + preisGes + "Euro.");
		}
		else {
			System.out.println("Die Lieferung ist gratis.");
			float preisGes = mausAnzahl *(preisMaus+(preisMaus*mehrwertSt));
			System.out.println("Der Gesamtpreis mit Steuer ist " + preisGes + " Euro.");
		}
	}

}
